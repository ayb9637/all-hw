
// Задание 1. Сокращенная форма записи цикла if else с помощью тернарных операторов
// Для проверки работы кода с использованием тернарного оператора, было создано переменные с разними типа. Для выведения результата использовано document.write.
let a = 1;
let b = 6;
let result;
let c = 'Мало';
let d = 'Много';

// if (a + b < 4) {
//     result = 'Мало';
// } else {
//     result = 'Много';
// }

result = a + b < 4 ? c : d;
document.write (result);

document.write("<hr>");



//Задание 2. Сокращенная форма записи цикла if else с помощью нескольких тернарных операторов
//Для просмотра результата выполнения кода, было создано 2 дополнительные переменные + использовано функцию prompt, а так же добавлено console.log
let message;
let login;
login = prompt ('Введите логин');


// if (login == 'Вася') {
//    console.log (message = 'Привет');
// } else if (login == 'Директор') {
//     console.log (message = 'Здравствуйте');
// } else if (login == ''){
//     console.log (message = 'Нет логина');
// } else {
//     message = '';
// }

message = login == 'Вася' ? alert('Привет'): login == 'Директор' ? alert('Здравствуйте'): login == ''? alert('Нет логина'): '';

//Задание 3.
// Цикл выводит на экран сумму всех чисел в промежутке от А до В
let A = 2;
let B = 56;
let sum = 0;
// числа для переменных были заданы с целью просмотра корректности работы цикла.
for (let i = A; i <= B; i++) {
    sum += i;
 }
alert(sum);

//Вывод нечетных значений в промежутке от А до В 
    for (let i = A; i < B; i++) {
    if (i % 2 == 0) continue; // если остаток равен нулю, пропускать эту цифру
    console.log(i); // при присвоении переменным А и В каких либо чисел, будут выводиться все нечетные значения в промежутке в консоль
  }

// Задание 4. 

// данный цикл выводит прямоугольник звездочками


let number = 5;
let numberTwo = 30;
for (let i = 0; i < number; i++)
{
    for (let j = 0; j < numberTwo; j++) {
        document.write ("*"); 
    }
    document.write ("</br>");
}
document.write("<hr>");



// данный цикл выводит прямоугольний треугольник


let numberThree = 15;
for (let i = 0; i < numberThree; i++)
{
    for (let j = 0; j < i+1; j++) {
        document.write ("*"); 
    }
    document.write ("</br>");
}
document.write("<hr>");

//данный цикл выводит равносторонний треугольник


let line = 6; 
let space = 7; 
let star = 1; 

for (let i = 0; i < line; i++) {
    for (let j = 0; j < space; j++) {
        document.write("&nbsp\n");
    }
    for (let j = 0; j < star; j++) {
        document.write("*");
    }
    space -= 1;
    star += 2;
    document.write("<br>");
}
    document.write("<hr>");

//данный цикл выводит ромб


    line = 8; 
    space = 7; 
    star = 1; 
for (let i = 0; i < line; i++) {
    for (let j = 0; j < space; j++) {
        document.write("&nbsp\n");
    }
    for (let j = 0; j < star; j++) {
        document.write("*");
    }
    space -= 1;
    star += 2;
    document.write("<br>");
}
    line = 8; 
    space = 1; 
    star = 13; 
for (let i = 0; i < line; i++) {
    
    for (let j = 0; j < space; j++) {
        document.write("&nbsp\n");
    }
    for (let j = 0; j < star; j++) {
        document.write("*");
    }
    space += 1;
    star -= 2;
    document.write("<br>");
}