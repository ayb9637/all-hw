function Calc() {

    this.read = function() { //для запроса значений
      this.numberOne = +prompt('Введите первое число'); 
      this.numberTwo = +prompt('Введите второе число');
    };
  
    this.sum = function() { // для подсчета суммы введеных значений
      return this.numberOne + this.numberTwo;
    };
  
    this.mul = function() { // для подсчета умножения введеных значений
      return this.numberOne * this.numberTwo;
    };
  }
 
  const calculator = new Calc();
  calculator.read();
  alert( "Сумма = " + calculator.sum() ); 
  alert( "Умножение = " + calculator.mul() ); // вывод с помощью модальных окон
