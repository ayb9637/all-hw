const HumaN = []; //создание масива Human в котором будут все объекты.
 HumaN[0] = new Human (10);
 HumaN[1] = new Human (34);
 HumaN[2] = new Human (65);
 HumaN[3] = new Human (24);
 HumaN[4] = new Human (23);

 function Human (age){ //функция для записи значений возраста
    this.age = age;
 }
 console.log('Значение от меньшего к большему');
 Human.prototype.sorting = (function(a, b){ 
    return a.age - b.age
    }
);

HumaN.sort(Human.prototype.sorting);
for (let i = 0; i < HumaN.length; i++) {    //цыкл for который выводит значения возраста до тех пор пока масив не закончиться
    console.log(HumaN[i].age);
}

console.log('Значение от большего к меньшему');

Human.prototype.sorting = (function(a, b){ 
    return b.age - a.age
    }
);

HumaN.sort(Human.prototype.sorting);
for (let i = 0; i < HumaN.length; i++) {    //цыкл for который выводит значения возраста до тех пор пока масив не закончиться
    console.log(HumaN[i].age);
}