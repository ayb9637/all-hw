const document1 = {  // создание обьекта документ
    title: "title",
    body: "body",
    footer: "footer",
    data: "data",

    application: { // создание вложеного обьекта приложение 
        titleApplication: "title",
        bodyApplication: "body",
        footerApplication: "footer",
        dataApplication: "data",
    },
};

for (let elem in document1) {         // создание той переменной, которая будет перебирать свойства в объекте document1
   
    if (elem == "application") { 
        document.write ('*********************************************************************' + '</br>')
        for (let elem in document1.application) {         // создание той переменной, которая будет перебирать свойства в вложеном объекте application
            document1.application[elem] = prompt('Введите' + ' ' + elem + ":"); //просим пользователя ввести данные для свойств вложеного обьекта приложение
            document.write ('Свойство Приложения'+ ' ' + elem + ':' + ' ' + document1.application[elem] + '</br>') //выводим данные вложеного обьекта приложение
        }
    } else {
        document1[elem] = prompt('Введите' + ' ' + elem + ":"); //просим пользователя ввести данные для свойств обьекта документ
        document.write ('Свойство Документа'+ ' ' + elem + ':' + ' ' + document1[elem] + '</br>')//выводим данные обьекта документ
    }

}


